# ICME 2023 Supplementary Material

## Comparison of HDR quality metrics in Per-Clip Lagrangian multiplier optimisation with AV1

Vibhoothi$^{\dagger}$, François Pitié$^{\dagger}$, Angeliki Katsenou$^{\dagger}$, Yeping Su$^{\ddagger}$, Balu Adsumilli$^{\ddagger}$, Anil Kokaram$^{\dagger}$

$^{\dagger}$ Sigmedia Group, Department of Electronic and Electrical
Engineering, *Trinity College Dublin*, Ireland

$^{\ddagger}$ YouTube Media-Algorithms Team, *Google Inc*, California, USA

$^{\dagger}$\{vibhoothi, pitief, akatsenou, anil.kokaram\}at tcd dot ie

$^{\ddagger}$ \{yeping,badsumilli\} at google dot com


Conference Homepage: [ICME 2023
](https://2023.ieeeicme.org/)


## Table of Contents
- [ICME 2023 Supplementary Material](#icme-2023-supplementary-material)
  - [Comparison of HDR quality metrics in Per-Clip Lagrangian multiplier optimisation with AV1](#comparison-of-hdr-quality-metrics-in-per-clip-lagrangian-multiplier-optimisation-with-av1)
  - [Table of Contents](#table-of-contents)
  - [Abstract](#abstract)
  - [Dataset](#dataset)
  - [Encoder Configuration](#encoder-configuration)
  - [Quantization Parameters(QP)](#quantization-parametersqp)
  - [*Encoder Modifications*](#encoder-modifications)
  - [*Optimiser Setup*](#optimiser-setup)
  - [Data Collection](#data-collection)
  - [Experimental Results](#experimental-results)
    - [Summary results](#summary-results)
    - [Meridian](#meridian)
    - [SVT](#svt)
    - [Cables-4K](#cables-4k)
    - [Nocturne](#nocturne)
    - [Sparks](#sparks)
    - [Sol-Levante](#sol-levante)
    - [Cosmos](#cosmos)
  - [Correlation scores between metrics](#correlation-scores-between-metrics)
  - [Reference](#reference)

## Abstract

The complexity of modern codecs along with the increased need of delivering high-quality videos at low bitrates has reinforced the idea of a per-clip tailoring of parameters for optimised rate-distortion performance.
While the objective quality metrics used for Standard Dynamic Range (SDR) videos have been well studied, the transitioning of consumer displays to support High Dynamic Range (HDR) videos, poses a new challenge to rate-distortion optimisation.
In this paper, we review the popular HDR metrics DeltaE100 (DE100), PSNRL100, wPSNR, and HDR-VQM. We measure the impact of employing these metrics in per-clip direct search optimisation of the rate-distortion Lagrange multiplier $\lambda$ in AV1. We report, on 35 HDR videos, average Bjontegaard Delta Rate (BD-Rate) gains of 4.675\%, 2.226\%, and 7.253\% in terms of DE100, PSNRL100, and HDR-VQM. We also show that the inclusion of chroma in the quality metrics has a profound impact on optimisation, which can only be partially addressed by the use of chroma offsets.


## Dataset

We have used the Dataset from our previous work [1.][2.] which contains 50
4K HDR sequences, in this work we are using a subset of them @ 35, these can be
also represented as "Shot-group" as shown in the orignial paper. This is shown
below for reference.

<Details>

|#|Video|Video Set|Shot-group|Resolution |Framerate|
|:----|:----|:----|:----|:----|:----|
|1|aom_cosmos_3840x2160_24_hdr10_11589-11752.y4m|AOM-Candidates|Cosmos|1920x1080|24|
|2|aom_cosmos_3840x2160_24_hdr10_12916-13078.y4m|AOM-Candidates|Cosmos|1920x1080|24|
|3|aom_cosmos_3840x2160_24_hdr10_1573-1749.y4m|AOM-Candidates|Cosmos|1920x1080|24|
|4|aom_cosmos_3840x2160_24_hdr10_8686-8826.y4m|AOM-Candidates|Cosmos|1920x1080|24|
|5|aom_cosmos_3840x2160_24_hdr10_9561-9789.y4m|AOM-Candidates|Cosmos|1920x1080|24|
|6|aom_meridian_3840x2160_5994_hdr10_12264-12745.y4m|AOM-Candidates|Meridian |1920x1080|59.94|
|7|aom_meridian_3840x2160_5994_hdr10_15932-16309.y4m|AOM-Candidates|Meridian |1920x1080|59.94|
|8|aom_meridian_3840x2160_5994_hdr10_1782-2163.y4m|AOM-Candidates|Meridian |1920x1080|59.94|
|9|aom_meridian_3840x2160_5994_hdr10_22412-22738.y4m|AOM-Candidates|Meridian |1920x1080|59.94|
|10|aom_meridian_3840x2160_5994_hdr10_24058-24550.y4m|AOM-Candidates|Meridian |1920x1080|59.94|
|11|aom_nocturne_3840x2160_60_hdr10_17140-17709.y4m|AOM-Candidates|Nocturne|1920x1080|60|
|12|aom_nocturne_3840x2160_60_hdr10_27740-28109.y4m|AOM-Candidates|Nocturne|1920x1080|60|
|13|aom_nocturne_3840x2160_60_hdr10_32660-32799.y4m|AOM-Candidates|Nocturne|1920x1080|60|
|14|aom_nocturne_3840x2160_60_hdr10_9010-9349.y4m|AOM-Candidates|Nocturne|1920x1080|60|
|15|aom_sol_levante_3840x2160_24_hdr10_2268-2412.y4m|AOM-Candidates|Sol-Levante|1920x1080|24|
|16|aom_sol_levante_3840x2160_24_hdr10_289-453.y4m|AOM-Candidates|Sol-Levante|1920x1080|24|
|17|aom_sol_levante_3840x2160_24_hdr10_3282-3874.y4m|AOM-Candidates|Sol-Levante|1920x1080|24|
|18|aom_sol_levante_3840x2160_24_hdr10_4123-4545.y4m|AOM-Candidates|Sol-Levante|1920x1080|24|
|19|aom_sparks_4096x2160_5994_hdr10_11198-11570.y4m|AOM-Candidates|Sparks|1920x1080|59.94|
|20|aom_sparks_4096x2160_5994_hdr10_350-480.y4m|AOM-Candidates|Sparks|1920x1080|59.94|
|21|aom_sparks_4096x2160_5994_hdr10_6026-6502.y4m|AOM-Candidates|Sparks|1920x1080|59.94|
|22|aom_sparks_4096x2160_5994_hdr10_8396-8941.y4m|AOM-Candidates|Sparks|1920x1080|59.94|
|23|MeridianRoad_3840x2160_5994_hdr10.y4m|AOM-CTC|Meridian |1920x1080|59.94|
|24|NocturneDance_3840x2160_60fps_hdr10.y4m|AOM-CTC|Nocturne|1920x1080|60|
|25|NocturneRoom_3840x2160_60fps_hdr10.y4m|AOM-CTC|Nocturne|1920x1080|60|
|26|SparksWelding_4096x2160_5994_hdr10.y4m|AOM-CTC|Sparks|1920x1080|59.94|
|27|svt_midnight_sun_3840x2160_50_10bit_420p.y4m|svt-opencontent|SVT|1920x1080|50|
|28|svt_smithy_3840x2160_50_10bit_420p.y4m|svt-opencontent|SVT|1920x1080|50|
|29|svt_smoke_sauna_3840x2160_50_10bit_420p.y4m|svt-opencontent|SVT|1920x1080|50|
|30|svt_water_flyover_3840x2160_50_10bit_420p.y4m|svt-opencontent|SVT|1920x1080|50|
|31|svt_waterfall_3840x2160_50_10bit_420p.y4m|svt-opencontent|SVT|1920x1080|50|
|32|wipe_3840x2160_5994_10bit_420_1747-1877.y4m|Cables-4K|Wipe|1920x1080|59.94|
|33|wipe_3840x2160_5994_10bit_420_3857-3987.y4m|Cables-4K|Wipe|1920x1080|59.94|
|34|wipe_3840x2160_5994_10bit_420_8750-8880.y4m|Cables-4K|Wipe|1920x1080|59.94|
|35|wipe_3840x2160_5994_10bit_420_881-1011.y4m|Cables-4K|Wipe|1920x1080|59.94|


</Details>

Classification of shot groups:

| Shot Group                                                                                                         | Description                                                  |
| ------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------ |
| Cosmos<sup>[1](https://opencontent.netflix.com/)</sup>                                                             | Vibrant animated sequence, high temporal complexity, 24fps   |
| Meridian<sup>[1](https://opencontent.netflix.com/),[2](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)</sup>    | Natural sequence, high spatial complexity, 59.94fps          |
| Nocturne<sup>[1](https://opencontent.netflix.com/),[2](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)</sup>    | Natural sequence, medium spatial complexity, 60fps           |
| Sol Levante<sup>[1](https://opencontent.netflix.com/),[2](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)</sup> | Animated sequence, medium-high temporal complexity, 24fps    |
| Sparks<sup>[1](https://opencontent.netflix.com/),[2](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)</sup>      | Sequence with medium motion and wide dynamic range, 59.94fps |
| SVT<sup>[3](https://www.svt.se/open/en/content/)</sup>                                                             | Very high natural complexity sequences, 50fps                |
| Cables 4K<sup>[4](https://www.cablelabs.com/4k)</sup>                                                              | Outdoor sequences with moderate complexity, 59.94fps         |

Sample frames from the subset of the dataset which is tonemapped to BT.709 for
representation is shown below,

![Frame sequences](https://i.imgur.com/9tPPvEr.jpg)

> Note: Sequences are tonemapped to BT.709 for better representation,
```shell
ffmpeg -i $input.y4m -vf zscale=tin=smpte2084:min=bt2020nc:pin=bt2020:rin=tv:t=smpte2084:m=bt2020nc:p=bt2020:r=tv,zscale=t=linear:npl=100,format=gbrpf32le,zscale=p=bt709,tonemap=tonemap=hable:desat=0,zscale=t=bt709:m=bt709:r=tv,format=yuv420p png/test%04d.png
```

The source for these clips are:
1) [Netflix Open Content](https://opencontent.netflix.com/), also available at
[AOM-CTC](https://media.xiph.org/video/aomctc/test_set/hdr1_4k/),
[AOM-Candidates](https://media.xiph.org/video/av2/), [Netflix](http://download.opencontent.netflix.com.s3.amazonaws.com/index.html?prefix=aom_test_materials/HDR/).
1) [Cables 4K](https://www.cablelabs.com/4k)
2) [SVT Open Content](https://www.svt.se/open/en/content/)


## Encoder Configuration

libaom-av1-3.2.0, [287164d](https://aomedia.googlesource.com/aom/+/287164d)

Random-Access Preset as per [AOM-CTC](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)
``` sh
$AOMEMC --cpu-used=0 --passes=1 --lag-in-frames=19 --auto-alt-ref=1 --min-gf-interval=16 --max-gf-interval=16
--gf-min-pyr-height=4 --gf-max-pyr-height=4 --limit=130 --kf-min-dist=65 --kf-max-dist=65 --use-fixed-qp-offsets=1
--deltaq-mode=0 --enable-tpl-model=0 --end-usage=q --cq-level=$Q --enable-keyframe-filtering=0 --threads=1
--test-decode=fatal -o output.ivf --color-primaries=bt2020 --transfer-characteristics=smpte2084
--matrix-coefficients=bt2020ncl --chroma-sample-position=colocated $OPTIONS $INPUT.Y4M
```
> GOP-Size is fixed at 65, Sub-GOP is 16, Temporal-layers is 4, Closed-GOP
GOP-Size is set in AV1 with help of `kf-min-dist` and `kf-max-dist`. Sub-GOP
size is set in AV1 using `min-gf-interval` and `max-gf-interval`. The temporal
layers is defined using `gf-min-pyr-height` and `gf-max-pyr-height`. To have a
hierarchical reference structure, the number of temporal layers should be set to
$log2(SubGopsize)$. Open-GOP and Closed GOP is controlled with help of `
--enable-fwd-kf` inside AV1. Color Primaries, Transfer Charecteristics, Matrix
Coffecients are also specified which is required HDR Metadata.

All-Intra Preset as per [AOM-CTC](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)
``` sh
$AOMENC --codec=av1 --cpu-used=0 --passes=1 --end-usage=q --cq-level=$x --kf-min-dist=0 --kf-max-dist=0 --use-fixed-qp-offsets=1 --deltaq-mode=0 --enable-tpl-model=0 --enable-keyframe-filtering=0 --ivf --threads=1 --test-decode=fatal -o $output.ivf --cpu-used=6 --color-primaries=bt2020 --transfer-characteristics=smpte2084 --matrix-coefficients=bt2020ncl --chroma-sample-position=colocated --bit-depth=10 $input.y4m
```
> All Intra configuration, with GOP and Sub-GOP length to be 0, Color Primaries, Transfer Charecteristics, Matrix
Coffecients, Bitdepth is signalled as HDR Metadata.


## Quantization Parameters(QP)

| SL-No | libaom-av1 |
| ----- | ---------- |
| 1     | 27         |
| 2     | 39         |
| 3     | 49         |
| 4     | 59         |
| 5     | 63         |

The QP points were chosen based on the [AOM-Common Testing Configuration(CTC.)](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf).



## *Encoder Modifications*
In, libaom-AV1, We have modifed the release version to support feeding
$`\lambda`$ values via command-line argument.

For applying Chroma and Luma Offsets, we applied ChangeID
[I99757226aea84d415f3fcf405ef3f4d90bdbf7dd](https://aomedia-review.googlesource.com/q/I99757226aea84d415f3fcf405ef3f4d90bdbf7dd)
on top of the release tag v3.2.0 which we are using to allow us to control
chroma offsets via command-line parameters.


## *Optimiser Setup*
For this experiment, we using Powell method as adopted by [1.], which is implemented in Scipy's optimise module
[scipy.optimise.minimise](https://docs.scipy.org/doc/scipy/reference/optimize.minimize-powell.html#optimize-minimize-powell).

## Data Collection

The RD_points are computed using
[libvmaf v0.2.1](https://github.com/Netflix/vmaf/tree/v2.2.1/libvmaf). For HDR
metrics, we are using HDRTools [v0.22 release branch](https://gitlab.com/standards/HDRTools/-/tree/0.22-dev)


| SL No | Metrics Name | SL No    | Metrics Name  |
| ----- | ------------ | -------- | ------------- |
| 1     | PSNR Y       | 9        | PSNR-HVS Cb   |
| 2     | PSNR Cb      | 10       | PSNR-HVS Cr   |
| 3     | PSNR Cr      | 11       | Encoding Time |
| 4     | PSNR-HVS     | 12       | Decoding Time |
| 5     | CIEDE2000    | 13       | VMAF          |
| 6     | SSIM         | 14       | VMAF-NEG      |
| 7     | MS-SSIM      | 15       | APSNR Y       |
| 8     | PSNR-HVS Y   | 16       | APSNR Cb      |
| 17    | APSNR Cr     | 18       | DE100         |
| 19    | PSNRL100     | 20       | wPSNR-Y       |
| 21    | wPSNR-U      | 22       | wPSNR-V       |
| 23    | HDR-VQM      |          |               |

The configuration files used to compute the metrics is adopted from the [3GPP
study](https://github.com/haudiobe/5GVideo/). This is available in the
[cfg](data/cfg/) folder of this material.

For ease of computation, we have also provided 3 scipts for computing them,
- HDR-VQM: `./HDR_VQM.sh $SRC-%05d.exr W H FPS NF $DST-%05d.exr`
- DeltaE+L100: `./HDRMetrics.sh $SRC-%05d.exr W H FPS NF $DST-%05d.exr`
- wPSNR: `./HDRMetrics_psnr.sh $SRC.yuv W H FPS NF $DST.yuv`

Sample command: `./HDR_VQM.sh aom_cosmos_3840x2160_24_hdr10_1573-1749.y4m-src-%05d.exr 3840 2160 24 130 aom_cosmos_3840x2160_24_hdr10_1573-1749.y4m-compressed-%05d.exr`


## Experimental Results

In this material, we are having the results in multiple forms, i) full summary,
same as paper. ii) Results grouped by shot.


### Summary results

**Average across the full-dataset: Table belows shows results organised for the whole dataset&**
|Evaluation Metrics| |Optimisation Metric| | | | |Chroma Offset (CO)| | |
|:----|:----|:----|:----|:----|:----|:----|:----|:----|:----|
|Metric|DR-Plane|MS_SSIM|PSNRL100|DE100|wPSNR_AVG|HDR_VQM|Baseline+CO|DE100 + CO|L100 + CO|
|MS_SSIM|SDR-Luma|-2.122|-1.298|1.253|0.112|0.116|1.258|2.722|-1.941|
|PSNRL100|HDR-Luma|-1.419|-2.226|0.556|-0.573|0.855|0.949|1.714|-2.867|
|DE100|HDR-All|3.660|0.602|-4.675|-3.646|5.044|-7.867|-9.067|0.293|
|WPSNR_AVG|HDR-All|2.917|0.524|-3.099|-3.347|3.893|-5.026|-7.336|-0.539|
|HDR_VQM|HDR-Luma|-1.352|-0.951|-1.802|-2.853|-7.253|1.192|-1.063|-2.326|
|VMAF|SDR-Luma|-1.478|-0.684|3.087|1.513|0.385|1.125|5.370|-1.224|
|WPSNR_Y|HDR-Luma|-1.720|-1.583|2.108|0.562|0.178|1.238|3.912|-1.957|
|WPSNR_U|HDR-Chroma|6.722|2.120|-6.579|-5.722|7.318|-9.437|-13.888|-0.010|
|WPSNR_V|HDR-Chroma|9.657|2.553|-8.903|-8.217|10.447|-9.381|-19.389|1.818|
|CIEDE2000|SDR-All|1.230|-0.477|-2.004|-2.448|2.763|-3.713|-3.663|-1.296|
|PSNR_Y|SDR-Luma|-1.688|-1.436|1.975|0.505|0.097|1.251|3.714|-1.787|
|PSNR_U|SDR-Chroma|6.876|2.113|-6.792|-5.836|7.334|-9.555|-14.016|0.279|
|PSNR_V|SDR-Chroma|10.042|2.658|-9.014|-8.360|11.580|-9.557|-19.632|1.937|
|PSNR_AVG|SDR-All|3.100|0.671|-3.249| |4.141|-5.070|-7.577|-0.299|


**Average across different content-group/shot-group**:


### Meridian

<Details>

|Evaluation Metrics| |Optimised Metric| | | | |Chroma Offset (CO)| | |
|:----|:----|:----|:----|:----|:----|:----|:----|:----|:----|
|Metric|DR-Plane|MS_SSIM|PSNRL100|DE100|wPSNR_AVG|HDR_VQM|Baseline+CO|DE100 + CO|L100 + CO|
|MS_SSIM|SDR-Luma|-1.510|-1.332|4.079|2.109|2.320|0.716|6.681|-1.156|
|PSNRL100|HDR-Luma|-1.543|-0.960|5.164|2.922|1.470|0.727|6.445|-2.451|
|DE100|HDR-All|2.159|2.939|-2.522|-2.201|6.013|-8.444|-12.281|1.755|
|WPSNR_AVG|HDR-All|1.729|2.382|-2.592|-2.936|2.266|-4.820|-13.643|-1.444|
|HDR_VQM|HDR-Luma|1.095|2.837|-1.176|-1.918|-6.797|-0.114|1.252|1.758|
|VMAF|SDR-Luma|-0.952|-0.509|4.335|2.439|1.720|0.544|8.831|-0.808|
|WPSNR_Y|HDR-Luma|-1.343|-1.194|4.227|2.284|1.391|0.727|7.165|-1.934|
|WPSNR_U|HDR-Chroma|3.309|5.420|-6.958|-6.568|2.264|-8.158|-25.061|-1.125|
|WPSNR_V|HDR-Chroma|3.511|4.763|-6.079|-5.617|4.914|-8.299|-29.120|-1.108|
|CIEDE2000|SDR-All|1.086|1.225|1.047|-0.229|2.831|-2.715|-2.680|-0.685|
|PSNR_Y|SDR-Luma|-1.214|-1.260|3.894|2.090|2.087|0.725|7.068|-1.334|
|PSNR_U|SDR-Chroma|4.010|5.533|-6.133|-5.818|3.584|-8.263|-23.603|0.767|
|PSNR_V|SDR-Chroma|4.164|4.533|-5.561|-5.500|6.588|-8.889|-27.954|0.967|
|PSNR_AVG|SDR-All|2.211|2.341|-2.266|-2.730|3.503|-5.016|-12.716|0.059|

</Details>

### SVT

<Details>

|Evaluation Metrics| |Optimised Metric| | | | |Chroma Offset (CO)| | |
|:----|:----|:----|:----|:----|:----|:----|:----|:----|:----|
|Metric|DR-Plane|MS_SSIM|PSNRL100|DE100|wPSNR_AVG|HDR_VQM|Baseline+CO|DE100 + CO|L100 + CO|
|MS_SSIM|SDR-Luma|-0.818|0.541|-0.857|-1.183|1.056|0.745|0.916|-3.405|
|PSNRL100|HDR-Luma|0.810|-0.462|0.601|-1.007|1.738|0.669|2.033|-2.395|
|DE100|HDR-All|5.911|-0.263|-11.179|-7.655|6.650|-4.622|-18.251|-3.431|
|WPSNR_AVG|HDR-All|4.611|-0.063|-6.078|-4.107|6.618|-4.026|-11.241|-1.136|
|HDR_VQM|HDR-Luma|-1.388|-2.044|-3.857|-3.922|-4.671|1.080|-1.724|-5.917|
|VMAF|SDR-Luma|0.187|-0.118|4.693|1.170|1.839|0.639|6.956|-2.416|
|WPSNR_Y|HDR-Luma|1.154|-0.298|3.705|0.592|1.849|0.613|5.369|-1.674|
|WPSNR_U|HDR-Chroma|5.613|1.002|-11.066|-4.562|10.762|-9.470|-18.418|-1.919|
|WPSNR_V|HDR-Chroma|19.098|-2.800|-26.784|-17.923|13.567|-5.707|-36.142|6.592|
|CIEDE2000|SDR-All|2.055|-0.135|-6.022|-4.492|4.308|-2.738|-7.927|-3.809|
|PSNR_Y|SDR-Luma|0.782|-0.323|2.373|-0.090|1.197|0.669|3.863|-2.154|
|PSNR_U|SDR-Chroma|5.216|0.535|-13.158|-5.563|10.297|-9.767|-21.363|-4.053|
|PSNR_V|SDR-Chroma|17.034|-3.246|-27.536|-18.164|11.914|-6.178|-37.163|5.077|
|PSNR_AVG|SDR-All|3.918|-0.177|-7.727|-4.843|6.052|-4.178|-13.599|-2.234|

</Details>

### Cables-4K

<Details>

|Evaluation Metrics| |Optimised Metric| | | | |Chroma Offset (CO)| | |
|:----|:----|:----|:----|:----|:----|:----|:----|:----|:----|
|Metric|DR-Plane|MS_SSIM|PSNRL100|DE100|wPSNR_AVG|HDR_VQM|Baseline+CO|DE100 + CO|L100 + CO|
|MS_SSIM|SDR-Luma|-1.287|-1.111|1.008|-0.752|-0.806|1.827|3.185|-1.959|
|PSNRL100|HDR-Luma|-1.077|-1.140|0.744|-0.869|-0.408|1.601|2.789|-1.878|
|DE100|HDR-All|-1.739|-2.102|-6.273|-5.871|-2.550|-7.649|-11.003|-4.566|
|WPSNR_AVG|HDR-All|-1.585|-1.911|-3.997|-4.948|-1.841|-4.872|-7.106|-3.650|
|HDR_VQM|HDR-Luma|-2.455|-2.782|-4.830|-4.508|-4.242|1.160|-2.109|-4.316|
|VMAF|SDR-Luma|-0.166|0.306|5.012|2.299|0.855|1.678|8.134|-0.324|
|WPSNR_Y|HDR-Luma|-0.554|-0.591|3.151|0.555|0.234|1.800|5.644|-0.874|
|WPSNR_U|HDR-Chroma|-1.957|-2.167|-6.849|-6.891|-2.301|-10.202|-12.428|-5.419|
|WPSNR_V|HDR-Chroma|-1.805|-2.895|-11.569|-10.241|-3.209|-8.159|-20.228|-5.641|
|CIEDE2000|SDR-All|-1.640|-1.738|-3.118|-3.778|-1.871|-4.713|-4.766|-3.707|
|PSNR_Y|SDR-Luma|-0.624|-0.612|3.143|0.568|0.240|1.807|5.524|-0.867|
|PSNR_U|SDR-Chroma|-1.908|-2.016|-7.026|-6.770|-2.211|-10.042|-12.524|-5.502|
|PSNR_V|SDR-Chroma|-1.871|-2.846|-11.601|-10.041|-3.201|-8.167|-20.329|-5.909|
|PSNR_AVG|SDR-All|-1.613|-1.872|-4.134|-4.937|-1.842|-4.843|-7.353|-3.741|

</Details>

### Nocturne

<Details>

|Evaluation Metrics| |Optimised Metric| | | | |Chroma Offset (CO)| | |
|:----|:----|:----|:----|:----|:----|:----|:----|:----|:----|
|Metric|DR-Plane|MS_SSIM|PSNRL100|DE100|wPSNR_AVG|HDR_VQM|Baseline+CO|DE100 + CO|L100 + CO|
|MS_SSIM|SDR-Luma|-3.891|-1.750|1.861|0.625|-2.002|1.816|3.974|-1.745|
|PSNRL100|HDR-Luma|-1.473|-4.911|-2.317|-2.946|3.220|1.703|-1.025|-4.894|
|DE100|HDR-All|7.316|-1.513|-4.759|-4.164|13.684|-6.707|-8.264|-1.939|
|WPSNR_AVG|HDR-All|5.420|-1.433|-3.868|-3.869|11.538|-5.923|-7.708|-2.558|
|HDR_VQM|HDR-Luma|-2.314|0.563|3.486|-0.462|-15.331|3.941|2.002|-0.417|
|VMAF|SDR-Luma|-3.678|-0.743|4.349|2.712|-2.480|1.163|7.829|-0.531|
|WPSNR_Y|HDR-Luma|-3.617|-2.379|1.244|0.242|-1.212|1.938|3.608|-2.498|
|WPSNR_U|HDR-Chroma|14.134|-1.017|-8.098|-7.123|20.663|-9.945|-15.557|-4.213|
|WPSNR_V|HDR-Chroma|15.028|-1.208|-10.969|-9.722|30.803|-12.785|-22.600|-5.102|
|CIEDE2000|SDR-All|2.129|-3.645|-3.196|-3.845|7.790|-4.212|-4.596|-2.973|
|PSNR_Y|SDR-Luma|-3.545|-1.684|1.903|0.894|-1.727|1.923|4.436|-1.781|
|PSNR_U|SDR-Chroma|12.734|-1.517|-8.327|-7.356|18.916|-9.999|-15.192|-4.253|
|PSNR_V|SDR-Chroma|14.897|-2.060|-12.506|-10.905|32.353|-12.808|-24.723|-6.399|
|PSNR_AVG|SDR-All|5.294|-1.330|-3.930|-3.846|11.183|-5.920|-7.788|-2.458|

</Details>

### Sparks

<Details>

|Evaluation Metrics| |Optimised Metric| | | | |Chroma Offset (CO)| | |
|:----|:----|:----|:----|:----|:----|:----|:----|:----|:----|
|Metric|DR-Plane|MS_SSIM|PSNRL100|DE100|wPSNR_AVG|HDR_VQM|Baseline+CO|DE100 + CO|L100 + CO|
|MS_SSIM|SDR-Luma|-1.812|-0.875|1.933|-0.604|-0.755|0.716|1.011|-1.113|
|PSNRL100|HDR-Luma|-1.234|-1.533|0.499|-0.831|-0.525|0.525|0.789|-1.799|
|DE100|HDR-All|1.067|-0.621|-2.372|-1.060|3.265|-11.072|-3.699|-0.370|
|WPSNR_AVG|HDR-All|0.225|-0.442|-0.149|-2.155|0.811|-4.729|-1.114|-0.729|
|HDR_VQM|HDR-Luma|-3.490|-2.453|-0.128|-2.224|-3.178|0.599|-1.261|-3.257|
|VMAF|SDR-Luma|0.049|0.604|2.044|0.568|0.750|0.781|1.558|0.388|
|WPSNR_Y|HDR-Luma|-0.843|-0.507|1.690|-0.351|-0.649|0.663|1.956|-0.599|
|WPSNR_U|HDR-Chroma|1.395|-0.252|-2.164|-3.850|2.282|-9.575|-4.277|-1.048|
|WPSNR_V|HDR-Chroma|1.324|-0.570|-1.630|-4.320|2.266|-11.513|-4.569|-1.100|
|CIEDE2000|SDR-All|-0.563|-1.475|-0.307|-1.994|0.917|-3.818|-2.453|-2.196|
|PSNR_Y|SDR-Luma|-0.950|-0.446|1.819|-0.320|-0.523|0.682|1.664|-0.656|
|PSNR_U|SDR-Chroma|1.256|-0.609|-1.890|-4.070|1.902|-9.736|-4.232|-1.359|
|PSNR_V|SDR-Chroma|1.398|-0.801|-1.249|-4.269|1.983|-11.633|-4.112|-1.392|
|PSNR_AVG|SDR-All|0.094|-0.541|0.155|-2.119|0.733|-4.605|-1.103|-0.897|

</Details>


### Sol-Levante

<Details>

|Evaluation Metrics| |Optimised Metric| | | | |Chroma Offset (CO)| | |
|:----|:----|:----|:----|:----|:----|:----|:----|:----|:----|
|Metric|DR-Plane|MS_SSIM|PSNRL100|DE100|wPSNR_AVG|HDR_VQM|Baseline+CO|DE100 + CO|L100 + CO|
|MS_SSIM|SDR-Luma|-3.008|-2.580|-0.271|0.557|2.865|1.081|1.961|-2.508|
|PSNRL100|HDR-Luma|-2.678|-3.293|-0.944|-0.512|1.729|0.320|0.770|-3.317|
|DE100|HDR-All|10.597|5.798|-2.353|-1.970|5.960|-8.029|-4.904|9.732|
|WPSNR_AVG|HDR-All|8.726|4.587|-2.181|-3.162|6.326|-4.731|-5.296|5.241|
|HDR_VQM|HDR-Luma|1.186|-1.344|-2.939|-5.244|-11.444|0.422|-4.477|-2.303|
|VMAF|SDR-Luma|-3.009|-2.549|-0.030|1.091|2.834|1.029|2.290|-2.432|
|WPSNR_Y|HDR-Luma|-4.166|-3.754|-0.417|0.340|1.092|1.045|1.910|-3.577|
|WPSNR_U|HDR-Chroma|22.288|10.711|-5.801|-7.169|15.726|-9.637|-12.452|13.713|
|WPSNR_V|HDR-Chroma|29.412|17.988|-3.117|-7.380|21.431|-8.008|-12.274|17.615|
|CIEDE2000|SDR-All|2.386|0.204|-1.796|-1.610|2.771|-2.796|-1.837|1.458|
|PSNR_Y|SDR-Luma|-3.769|-3.685|-0.542|-0.067|0.641|1.061|1.454|-3.613|
|PSNR_U|SDR-Chroma|24.847|11.664|-6.425|-7.885|17.028|-10.101|-13.140|15.624|
|PSNR_V|SDR-Chroma|32.960|20.383|-2.532|-7.353|27.529|-8.199|-12.582|19.373|
|PSNR_AVG|SDR-All|10.242|5.457|-2.256|-3.557|7.531|-4.936|-5.788|6.136|

</Details>

### Cosmos

<Details>

|Evaluation Metrics| |Optimised Metric| | | | |Chroma Offset (CO)| | |
|:----|:----|:----|:----|:----|:----|:----|:----|:----|:----|
|Metric|DR-Plane|MS_SSIM|PSNRL100|DE100|wPSNR_AVG|HDR_VQM|Baseline+CO|DE100 + CO|L100 + CO|
|MS_SSIM|SDR-Luma|-2.308|-2.100|-0.025|-0.552|-1.521|1.983|0.226|-2.012|
|PSNRL100|HDR-Luma|-2.883|-2.999|-0.461|-1.036|-1.911|0.995|-0.175|-2.903|
|DE100|HDR-All|0.183|0.434|-3.537|-2.897|-0.968|-8.653|-4.142|1.941|
|WPSNR_AVG|HDR-All|1.291|0.892|-2.774|-2.510|-0.331|-5.851|-3.455|1.623|
|HDR_VQM|HDR-Luma|-2.299|-2.641|-3.074|-2.691|-3.822|1.360|-2.481|-3.041|
|VMAF|SDR-Luma|-2.485|-1.979|0.466|-0.037|-1.931|2.239|0.746|-2.729|
|WPSNR_Y|HDR-Luma|-2.625|-2.512|0.608|-0.051|-1.230|1.914|1.088|-2.546|
|WPSNR_U|HDR-Chroma|2.852|1.974|-4.637|-3.967|-0.068|-9.422|-5.878|2.287|
|WPSNR_V|HDR-Chroma|4.736|3.829|-5.279|-4.711|0.483|-9.477|-6.944|5.107|
|CIEDE2000|SDR-All|2.666|2.403|-1.191|-1.454|0.653|-5.119|-1.248|3.121|
|PSNR_Y|SDR-Luma|-2.425|-2.317|0.597|-0.039|-1.130|1.938|1.082|-2.380|
|PSNR_U|SDR-Chroma|3.214|2.330|-4.384|-3.688|0.288|-9.353|-5.430|2.585|
|PSNR_V|SDR-Chroma|5.519|4.473|-5.031|-4.444|1.037|-9.281|-6.609|5.621|
|PSNR_AVG|SDR-All|1.777|1.329|-2.622|-2.329|0.030|-5.759|-3.217|2.004|

</Details>

## Correlation scores between metrics

| ![](/data/img/corr_pearson.png) | ![](/data/img/corr_spearman.png) | ![](/data/img/corr_kendall.png) |
|    :-:    |   :-:      |   :-:     |
| Pearson | Spearman | Kendall |

Heatmap denoting different correlation scores between Metrics. If the hue is towards red it means less correlation, and if it is more blue, it means higher correlation.

<Details>

```csvpreview {header="true"}
Base,Target,Pearson,Kendall,Spearman
PSNR-Y,PSNR-Y,1,1,1
PSNR-Y,PSNR-U,-0.46749,-0.3583,-0.50326
PSNR-Y,PSNR-V,-0.5288,-0.43431,-0.59402
PSNR-Y,CIEDE2000,-0.14597,-0.1982,-0.26431
PSNR-Y,MS-SSIM,0.85596,0.71099,0.83203
PSNR-Y,VMAF,0.88968,0.70105,0.86722
PSNR-Y,DE100,-0.42726,-0.3683,-0.4903
PSNR-Y,L100,0.68872,0.5895,0.69193
PSNR-Y,HDR-VQM,0.11614,0.27325,0.34223
PSNR-Y,wPSNR-Y,0.97201,0.87322,0.9538
PSNR-Y,wPSNR-U,-0.47924,-0.35135,-0.49927
PSNR-Y,wPSNR-V,-0.53825,-0.44011,-0.59775
PSNR-Y,wPSNR-AVG,-0.41832,-0.31145,-0.42076
PSNR-Y,PSNR-AVG,-0.42068,-0.31099,-0.4233
PSNR-U,PSNR-Y,-0.46749,-0.3583,-0.50326
PSNR-U,PSNR-U,1,1,1
PSNR-U,PSNR-V,0.89913,0.76387,0.90964
PSNR-U,CIEDE2000,0.70345,0.60193,0.77125
PSNR-U,MS-SSIM,-0.34349,-0.27045,-0.38802
PSNR-U,VMAF,-0.4748,-0.37146,-0.51824
PSNR-U,DE100,0.81177,0.69962,0.86573
PSNR-U,L100,-0.063178,-0.14327,-0.20797
PSNR-U,HDR-VQM,-0.087361,-0.024015,-0.041323
PSNR-U,wPSNR-Y,-0.44068,-0.33928,-0.48115
PSNR-U,wPSNR-U,0.99073,0.92713,0.98596
PSNR-U,wPSNR-V,0.89023,0.76972,0.9099
PSNR-U,wPSNR-AVG,0.92548,0.81047,0.93961
PSNR-U,PSNR-AVG,0.94114,0.82384,0.94599
PSNR-V,PSNR-Y,-0.5288,-0.43431,-0.59402
PSNR-V,PSNR-U,0.89913,0.76387,0.90964
PSNR-V,PSNR-V,1,1,1
PSNR-V,CIEDE2000,0.69424,0.53799,0.70844
PSNR-V,MS-SSIM,-0.35906,-0.32735,-0.45001
PSNR-V,VMAF,-0.54944,-0.4765,-0.63441
PSNR-V,DE100,0.84752,0.72578,0.89276
PSNR-V,L100,-0.05666,-0.19379,-0.26321
PSNR-V,HDR-VQM,-0.22379,-0.089175,-0.11934
PSNR-V,wPSNR-Y,-0.51624,-0.43173,-0.58628
PSNR-V,wPSNR-U,0.87944,0.73437,0.88383
PSNR-V,wPSNR-V,0.99174,0.93013,0.98213
PSNR-V,wPSNR-AVG,0.89808,0.76222,0.90519
PSNR-V,PSNR-AVG,0.92933,0.80398,0.94008
CIEDE2000,PSNR-Y,-0.14597,-0.1982,-0.26431
CIEDE2000,PSNR-U,0.70345,0.60193,0.77125
CIEDE2000,PSNR-V,0.69424,0.53799,0.70844
CIEDE2000,CIEDE2000,1,1,1
CIEDE2000,MS-SSIM,0.0020041,-0.088346,-0.098537
CIEDE2000,VMAF,-0.182,-0.23864,-0.33159
CIEDE2000,DE100,0.8192,0.60875,0.77034
CIEDE2000,L100,0.26249,0.053615,0.098204
CIEDE2000,HDR-VQM,-0.17703,0.062211,0.097891
CIEDE2000,wPSNR-Y,-0.13315,-0.17998,-0.24091
CIEDE2000,wPSNR-U,0.69065,0.56689,0.73671
CIEDE2000,wPSNR-V,0.70437,0.52605,0.69743
CIEDE2000,wPSNR-AVG,0.79577,0.59275,0.75535
CIEDE2000,PSNR-AVG,0.80394,0.61142,0.77505
MS-SSIM,PSNR-Y,0.85596,0.71099,0.83203
MS-SSIM,PSNR-U,-0.34349,-0.27045,-0.38802
MS-SSIM,PSNR-V,-0.35906,-0.32735,-0.45001
MS-SSIM,CIEDE2000,0.0020041,-0.088346,-0.098537
MS-SSIM,MS-SSIM,1,1,1
MS-SSIM,VMAF,0.79706,0.55436,0.69082
MS-SSIM,DE100,-0.2804,-0.26051,-0.34666
MS-SSIM,L100,0.61911,0.54026,0.62545
MS-SSIM,HDR-VQM,0.1623,0.34269,0.42977
MS-SSIM,wPSNR-Y,0.78709,0.64039,0.75912
MS-SSIM,wPSNR-U,-0.37705,-0.28579,-0.41261
MS-SSIM,wPSNR-V,-0.37625,-0.34498,-0.46946
MS-SSIM,wPSNR-AVG,-0.32215,-0.23453,-0.3206
MS-SSIM,PSNR-AVG,-0.28368,-0.21183,-0.28709
VMAF,PSNR-Y,0.88968,0.70105,0.86722
VMAF,PSNR-U,-0.4748,-0.37146,-0.51824
VMAF,PSNR-V,-0.54944,-0.4765,-0.63441
VMAF,CIEDE2000,-0.182,-0.23864,-0.33159
VMAF,MS-SSIM,0.79706,0.55436,0.69082
VMAF,VMAF,1,1,1
VMAF,DE100,-0.39204,-0.37201,-0.50204
VMAF,L100,0.51036,0.40479,0.49642
VMAF,HDR-VQM,0.14947,0.27151,0.34124
VMAF,wPSNR-Y,0.86025,0.6707,0.82631
VMAF,wPSNR-U,-0.4798,-0.36255,-0.4976
VMAF,wPSNR-V,-0.54674,-0.45715,-0.61063
VMAF,wPSNR-AVG,-0.41143,-0.32359,-0.43639
VMAF,PSNR-AVG,-0.42453,-0.34608,-0.46886
DE100,PSNR-Y,-0.42726,-0.3683,-0.4903
DE100,PSNR-U,0.81177,0.69962,0.86573
DE100,PSNR-V,0.84752,0.72578,0.89276
DE100,CIEDE2000,0.8192,0.60875,0.77034
DE100,MS-SSIM,-0.2804,-0.26051,-0.34666
DE100,VMAF,-0.39204,-0.37201,-0.50204
DE100,DE100,1,1,1
DE100,L100,-0.013214,-0.12699,-0.17293
DE100,HDR-VQM,-0.17281,-0.037556,-0.036573
DE100,wPSNR-Y,-0.41227,-0.36285,-0.48527
DE100,wPSNR-U,0.81186,0.69153,0.85931
DE100,wPSNR-V,0.87484,0.74502,0.90556
DE100,wPSNR-AVG,0.91365,0.7434,0.8989
DE100,PSNR-AVG,0.90838,0.75313,0.90219
L100,PSNR-Y,0.68872,0.5895,0.69193
L100,PSNR-U,-0.063178,-0.14327,-0.20797
L100,PSNR-V,-0.05666,-0.19379,-0.26321
L100,CIEDE2000,0.26249,0.053615,0.098204
L100,MS-SSIM,0.61911,0.54026,0.62545
L100,VMAF,0.51036,0.40479,0.49642
L100,DE100,-0.013214,-0.12699,-0.17293
L100,L100,1,1,1
L100,HDR-VQM,-0.14427,0.2078,0.23849
L100,wPSNR-Y,0.76741,0.63118,0.7633
L100,wPSNR-U,-0.057039,-0.13477,-0.19466
L100,wPSNR-V,-0.06114,-0.18873,-0.25647
L100,wPSNR-AVG,0.028377,-0.090404,-0.11609
L100,PSNR-AVG,-0.009261,-0.11484,-0.15051
HDR-VQM,PSNR-Y,0.11614,0.27325,0.34223
HDR-VQM,PSNR-U,-0.087361,-0.024015,-0.041323
HDR-VQM,PSNR-V,-0.22379,-0.089175,-0.11934
HDR-VQM,CIEDE2000,-0.17703,0.062211,0.097891
HDR-VQM,MS-SSIM,0.1623,0.34269,0.42977
HDR-VQM,VMAF,0.14947,0.27151,0.34124
HDR-VQM,DE100,-0.17281,-0.037556,-0.036573
HDR-VQM,L100,-0.14427,0.2078,0.23849
HDR-VQM,HDR-VQM,1,1,1
HDR-VQM,wPSNR-Y,0.051567,0.22427,0.27615
HDR-VQM,wPSNR-U,-0.09084,-0.024894,-0.041775
HDR-VQM,wPSNR-V,-0.21654,-0.078792,-0.10363
HDR-VQM,wPSNR-AVG,-0.17545,-0.031283,-0.03115
HDR-VQM,PSNR-AVG,-0.16855,-0.034501,-0.037015
wPSNR-Y,PSNR-Y,0.97201,0.87322,0.9538
wPSNR-Y,PSNR-U,-0.44068,-0.33928,-0.48115
wPSNR-Y,PSNR-V,-0.51624,-0.43173,-0.58628
wPSNR-Y,CIEDE2000,-0.13315,-0.17998,-0.24091
wPSNR-Y,MS-SSIM,0.78709,0.64039,0.75912
wPSNR-Y,VMAF,0.86025,0.6707,0.82631
wPSNR-Y,DE100,-0.41227,-0.36285,-0.48527
wPSNR-Y,L100,0.76741,0.63118,0.7633
wPSNR-Y,HDR-VQM,0.051567,0.22427,0.27615
wPSNR-Y,wPSNR-Y,1,1,1
wPSNR-Y,wPSNR-U,-0.43568,-0.32445,-0.46012
wPSNR-Y,wPSNR-V,-0.5193,-0.42453,-0.57462
wPSNR-Y,wPSNR-AVG,-0.37489,-0.285,-0.382
wPSNR-Y,PSNR-AVG,-0.40863,-0.31047,-0.42395
wPSNR-U,PSNR-Y,-0.47924,-0.35135,-0.49927
wPSNR-U,PSNR-U,0.99073,0.92713,0.98596
wPSNR-U,PSNR-V,0.87944,0.73437,0.88383
wPSNR-U,CIEDE2000,0.69065,0.56689,0.73671
wPSNR-U,MS-SSIM,-0.37705,-0.28579,-0.41261
wPSNR-U,VMAF,-0.4798,-0.36255,-0.4976
wPSNR-U,DE100,0.81186,0.69153,0.85931
wPSNR-U,L100,-0.057039,-0.13477,-0.19466
wPSNR-U,HDR-VQM,-0.09084,-0.024894,-0.041775
wPSNR-U,wPSNR-Y,-0.43568,-0.32445,-0.46012
wPSNR-U,wPSNR-U,1,1,1
wPSNR-U,wPSNR-V,0.88158,0.75667,0.904
wPSNR-U,wPSNR-AVG,0.93462,0.82789,0.95175
wPSNR-U,PSNR-AVG,0.92752,0.80115,0.92415
wPSNR-V,PSNR-Y,-0.53825,-0.44011,-0.59775
wPSNR-V,PSNR-U,0.89023,0.76972,0.9099
wPSNR-V,PSNR-V,0.99174,0.93013,0.98213
wPSNR-V,CIEDE2000,0.70437,0.52605,0.69743
wPSNR-V,MS-SSIM,-0.37625,-0.34498,-0.46946
wPSNR-V,VMAF,-0.54674,-0.45715,-0.61063
wPSNR-V,DE100,0.87484,0.74502,0.90556
wPSNR-V,L100,-0.06114,-0.18873,-0.25647
wPSNR-V,HDR-VQM,-0.21654,-0.078792,-0.10363
wPSNR-V,wPSNR-Y,-0.5193,-0.42453,-0.57462
wPSNR-V,wPSNR-U,0.88158,0.75667,0.904
wPSNR-V,wPSNR-V,1,1,1
wPSNR-V,wPSNR-AVG,0.91612,0.78565,0.92809
wPSNR-V,PSNR-AVG,0.92915,0.79334,0.92764
wPSNR-AVG,PSNR-Y,-0.41832,-0.31145,-0.42076
wPSNR-AVG,PSNR-U,0.92548,0.81047,0.93961
wPSNR-AVG,PSNR-V,0.89808,0.76222,0.90519
wPSNR-AVG,CIEDE2000,0.79577,0.59275,0.75535
wPSNR-AVG,MS-SSIM,-0.32215,-0.23453,-0.3206
wPSNR-AVG,VMAF,-0.41143,-0.32359,-0.43639
wPSNR-AVG,DE100,0.91365,0.7434,0.8989
wPSNR-AVG,L100,0.028377,-0.090404,-0.11609
wPSNR-AVG,HDR-VQM,-0.17545,-0.031283,-0.03115
wPSNR-AVG,wPSNR-Y,-0.37489,-0.285,-0.382
wPSNR-AVG,wPSNR-U,0.93462,0.82789,0.95175
wPSNR-AVG,wPSNR-V,0.91612,0.78565,0.92809
wPSNR-AVG,wPSNR-AVG,1,1,1
wPSNR-AVG,PSNR-AVG,0.98194,0.9028,0.97035
PSNR-AVG,PSNR-Y,-0.42068,-0.31099,-0.4233
PSNR-AVG,PSNR-U,0.94114,0.82384,0.94599
PSNR-AVG,PSNR-V,0.92933,0.80398,0.94008
PSNR-AVG,CIEDE2000,0.80394,0.61142,0.77505
PSNR-AVG,MS-SSIM,-0.28368,-0.21183,-0.28709
PSNR-AVG,VMAF,-0.42453,-0.34608,-0.46886
PSNR-AVG,DE100,0.90838,0.75313,0.90219
PSNR-AVG,L100,-0.009261,-0.11484,-0.15051
PSNR-AVG,HDR-VQM,-0.16855,-0.034501,-0.037015
PSNR-AVG,wPSNR-Y,-0.40863,-0.31047,-0.42395
PSNR-AVG,wPSNR-U,0.92752,0.80115,0.92415
PSNR-AVG,wPSNR-V,0.92915,0.79334,0.92764
PSNR-AVG,wPSNR-AVG,0.98194,0.9028,0.97035
PSNR-AVG,PSNR-AVG,1,1,1
``` 
</Details>


## Reference

[1]: Vibhoothi, François Pitié, Angeliki Katsenou, Daniel Joseph Ringis, Yeping Su, Neil Birkbeck, Jessie Lin, Balu Adsumilli, and Anil Kokaram. "Direct optimisation of λ for HDR content adaptive transcoding in AV1." In Applications of Digital Image Processing XLV, vol. 12226, pp. 36-45. SPIE, 2022. https://doi.org/10.1117/12.2632272

[2]: SPIE 2022 Supplementary material https://gitlab.com/mindfreeze/spie2022