#Sample Command
# ./HDR_VQM.sh $SRC-%05d.exr W H FPS NF $DST-%05d.exr
HDR_TOOLS=HDRTools/build/bin/HDRVQM
CFG_FILE=HDRVQM.cfg
SRC_FILE=$1
SRC_WIDTH=$2
SRC_HEIGHT=$3
SRC_RATE=$4
NMR_FRAMES=$5
OPT_FILE=$6
$HDR_TOOLS -f $CFG_FILE \
 -p Input0File=$SRC_FILE \
 -p Input0Width=$SRC_WIDTH \
 -p Input0Height=$SRC_HEIGHT \
 -p Input0Rate=$SRC_RATE \
 -p Input1File=$OPT_FILE \
 -p Input1Width=$SRC_WIDTH \
 -p Input1Height=$SRC_HEIGHT \
 -p Input1Rate=$SRC_RATE \
 -p NumberOfFrames=$NMR_FRAMES
