# Sample Command,
# ./convertExr.sh $INPUT.YUV W H FPS NF $OUTPUT-05%d.exr
HDR_TOOLS=HDRTools/build/bin/HDRConvert
CFG_FILE=HDRConvertYCbCr420ToEXR2020.cfg
SRC_FILE=$1
SRC_WIDTH=$2
SRC_HEIGHT=$3
SRC_RATE=$4
NMR_FRAMES=$5
OPT_FILE=$6
$HDR_TOOLS -f $CFG_FILE \
 -p SourceFile=$SRC_FILE \
 -p OutputFile=$OPT_FILE \
 -p SourceWidth=$SRC_WIDTH \
 -p SourceHeight=$SRC_HEIGHT \
 -p SourceRate=$SRC_RATE \
 -p OutputRate=$SRC_RATE \
 -p NumberOfFrames=$NMR_FRAMES
